<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConsultoriosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConsultoriosTable Test Case
 */
class ConsultoriosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConsultoriosTable
     */
    public $Consultorios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.consultorios',
        'app.citas',
        'app.medicos',
        'app.pacientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Consultorios') ? [] : ['className' => ConsultoriosTable::class];
        $this->Consultorios = TableRegistry::get('Consultorios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Consultorios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
