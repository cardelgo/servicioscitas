<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CitasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CitasTable Test Case
 */
class CitasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CitasTable
     */
    public $Citas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.citas',
        'app.medicos',
        'app.pacientes',
        'app.consultorios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Citas') ? [] : ['className' => CitasTable::class];
        $this->Citas = TableRegistry::get('Citas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Citas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
