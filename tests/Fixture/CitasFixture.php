<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CitasFixture
 *
 */
class CitasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'medico_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'paciente_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'consultorio_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'comentarios' => ['type' => 'string', 'length' => 145, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'hora' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'fecha' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_citas_medicos1_idx' => ['type' => 'index', 'columns' => ['medico_id'], 'length' => []],
            'fk_citas_pacientes1_idx' => ['type' => 'index', 'columns' => ['paciente_id'], 'length' => []],
            'fk_citas_consultorios1_idx' => ['type' => 'index', 'columns' => ['consultorio_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_citas_consultorios1' => ['type' => 'foreign', 'columns' => ['consultorio_id'], 'references' => ['consultorios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_citas_medicos1' => ['type' => 'foreign', 'columns' => ['medico_id'], 'references' => ['medicos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_citas_pacientes1' => ['type' => 'foreign', 'columns' => ['paciente_id'], 'references' => ['pacientes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'medico_id' => 1,
            'paciente_id' => 1,
            'consultorio_id' => 1,
            'comentarios' => 'Lorem ipsum dolor sit amet',
            'hora' => '01:51:48',
            'fecha' => '2018-03-19'
        ],
    ];
}
