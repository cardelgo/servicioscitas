<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Medicos Controller
 *
 * @property \App\Model\Table\MedicosTable $Medicos
 *
 * @method \App\Model\Entity\Medico[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MedicosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
     	$this->paginate = [
            'limit' => 10,
            'order' => [
                'Medicos.id' => 'desc'
            ]
        ];
		
        $this->response->header('Access-Control-Allow-Origin', '*');
		
        $medicosObj = $this->Medicos->find("all", ['limit' => 10, "contain"=>["Especialidades"] ]);
                

        $this->set('medicos', $this->paginate($medicosObj));
        $this->set('_serialize', ['medicos']);
    }
	
	

    /**
     * View method
     *
     * @param string|null $id Medico id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $medico = $this->Medicos->get($id, [
            'contain' => ['Especialidades', 'Citas']
        ]);

        $this->set('medico', $medico);
    }
	
	public function deleteRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no borrado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
        //$this->request->allowMethod(['post', 'delete']);
		
		if ($this->request->is(['post', 'delete'])) {
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para borrar un registro debe enviar el respectivo Id';
				goto EndDeleteRest;
			}
					
			$medico = $this->Medicos->get($this->request->data["id"]);
			
			if ($this->Medicos->delete($medico)) {
				$data["msg"] = "Registro borrado correctamente";
				$data["result"] = true;
			} else {
				$data["msg"] = "Registro no borrado";
			}
			
			EndDeleteRest:
			echo json_encode($data);
		}
	
        die();
    }
	
	 public function viewRest($cedula = null)
    {
		$this->paginate = [
            'limit' => 1,
            'order' => [
                'Medicos.id' => 'desc'
            ]
        ];
		
		$this->response->header('Access-Control-Allow-Origin', '*');
		 
        $medico = $this->Medicos->find("all", ["contain"=>["Especialidades"]])->where(["cedula"=>$cedula])->hydrate(false);

        $this->set('medicos', $this->paginate($medico));
		$this->set('_serialize', ['medicos']);
    }
	
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $medico = $this->Medicos->newEntity();
        if ($this->request->is('post')) {
            $medico = $this->Medicos->patchEntity($medico, $this->request->getData());
            if ($this->Medicos->save($medico)) {
                $this->Flash->success(__('The medico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medico could not be saved. Please, try again.'));
        }
        $especialidades = $this->Medicos->Especialidades->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "descripcion"]);
		

        $this->set(compact('medico', 'especialidades'));
    }
	
	public function addRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
        $medico = $this->Medicos->newEntity();
		
        if ($this->request->is('post')) {
            $medico = $this->Medicos->patchEntity($medico, $this->request->getData());
			
		   $countErrores = count($medico->errors());
		 //  $errores = [];
		   if($countErrores > 0) {
			
			 foreach($medico->errors() as $key => $value) {
					foreach($value as $k => $v) {
						//$errores[$key] = $v;
						$data["msg"] = $v;
						break;
					}
			   }	
			   
		   } else if ($this->Medicos->save($medico)) {
				$data["result"] = true;
				$data["msg"] = "Registro guardaddo correctamente";
            }
           
		   echo json_encode($data);
        }
       
	   die();
    }

    /**
     * Edit method
     *
     * @param string|null $id Medico id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $medico = $this->Medicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $medico = $this->Medicos->patchEntity($medico, $this->request->getData());
            if ($this->Medicos->save($medico)) {
                $this->Flash->success(__('The medico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medico could not be saved. Please, try again.'));
        }
        $especialidades = $this->Medicos->Especialidades->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "descripcion"]);
        $this->set(compact('medico', 'especialidades'));
    }
	
	public function editRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no guardado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		if ($this->request->is(['patch', 'post', 'put'])) {
			
			//print_r($this->request->data);
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para editar un registro debe enviar el respectivo Id';
				goto EndEditRest;
			}			
			$medico = $this->Medicos->get($this->request->data["id"]);
			$medicoP = $this->Medicos->patchEntity($medico, $this->request->getData());				
			$countErrores = count($medicoP->errors());

			if($countErrores > 0) {	 
			
				foreach($medicoP->errors() as $key => $value) {
					foreach($value as $k => $v) {
						$data["msg"] = $v;
						break;
					}
				}	
				
			} else {
					
				if ($this->Medicos->save($medico)) {
					
					$data["msg"] = 'Registro editado correctamente';
					$data["result"] = true;
					
				} else {
					
					$data["msg"] = 'Registro no editado';
					
				}			
			}
			
			EndEditRest:
			echo json_encode($data);		
		}
		
		die();
    }

    /**
     * Delete method
     *
     * @param string|null $id Medico id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $medico = $this->Medicos->get($id);
        if ($this->Medicos->delete($medico)) {
            $this->Flash->success(__('The medico has been deleted.'));
        } else {
            $this->Flash->error(__('The medico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
