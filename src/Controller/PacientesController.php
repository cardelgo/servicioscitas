<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pacientes Controller
 *
 * @property \App\Model\Table\PacientesTable $Pacientes
 *
 * @method \App\Model\Entity\Especialidade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PacientesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
		$this->paginate = [
            'limit' => 10,
            'order' => [
                'Pacientes.id' => 'desc'
            ]
        ];
		
        $this->response->header('Access-Control-Allow-Origin', '*');
        $pacientesObj = $this->Pacientes->find("all", ['limit' => 10]);

	
		$this->set('pacientes', $this->paginate($pacientesObj));
		
        
        $this->set('_serialize', ['pacientes']);
    }

    /**
     * View method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewRest($id = null)
    {
		$this->paginate = [
            'limit' => 1,
            'order' => [
                'Pacientes.id' => 'desc'
            ]
        ];
		
		$this->response->header('Access-Control-Allow-Origin', '*');
		 
        $paciente = $this->Pacientes->find("all")->where(["id"=>$id])->hydrate(false);

        $this->set('pacientes', $this->paginate($paciente));
		$this->set('_serialize', ['pacientes']);
    }
	
	 public function view($id = null)
    {
        $paciente = $this->Pacientes->get($id, [
            'contain' => []
        ]);

        $this->set('paciente', $paciente);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $paciente = $this->Pacientes->newEntity();
        if ($this->request->is('post')) {
            $paciente = $this->Pacientes->patchEntity($paciente, $this->request->getData());
            if ($this->Pacientes->save($paciente)) {
                $this->Flash->success(__('The paciente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The paciente could not be saved. Please, try again.'));
        }
        $this->set(compact('paciente'));
    }
	
	public function addRest(){
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		$paciente = $this->Pacientes->newEntity();
       
	   if ($this->request->is('post')) {
		   
		   $paciente = $this->Pacientes->patchEntity($paciente, $this->request->getData());
			
		   $countErrores = count($paciente->errors());
		 //  $errores = [];
		   if($countErrores > 0) {
			  // $data["errors"] = $paciente->errors();
			   
			   foreach($paciente->errors() as $key => $value) {
					foreach($value as $k => $v) {
						//$errores[$key] = $v;
						$data["msg"] = $v;
						break;
					}
			   }		
		   } else {		   
				if ($this->Pacientes->save($paciente)) {
					$data["msg"] = "Registro guardado";
					$data["result"] = true;
				}
		   }
			echo json_encode($data);
        }
		
		die();
	}
	

    /**
     * Edit method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $paciente = $this->Pacientes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $paciente = $this->Pacientes->patchEntity($paciente, $this->request->getData());
            if ($this->Pacientes->save($paciente)) {
                $this->Flash->success(__('The paciente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The paciente could not be saved. Please, try again.'));
        }
        $this->set(compact('paciente'));
    }
	
	public function editRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no guardado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		if ($this->request->is(['patch', 'post', 'put'])) {
			
			//print_r($this->request->data);
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para editar un registro debe enviar el respectivo Id';
				goto EndEditRest;
			}
		
			$paciente = $this->Pacientes->get($this->request->data["id"]);		
		
			$paciente = $this->Pacientes->patchEntity($paciente, $this->request->getData());				
			$countErrores = count($paciente->errors());

			if($countErrores > 0) {	 
			
				foreach($paciente->errors() as $key => $value) {
					foreach($value as $k => $v) {
						$data["msg"] = $v;
						break;
					}
				}	
				
			} else {
				    
				if ($this->Pacientes->save($paciente)) {
					
					$data["msg"] = 'Registro editado correctamente';
					$data["result"] = true;
					
				} else {
					
					$data["msg"] = 'Registro no editado';
					
				}			
			}
			
			EndEditRest:
			echo json_encode($data);		
		}
		
		die();
    }

    /**
     * Delete method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paciente = $this->Pacientes->get($id);
        if ($this->Pacientes->delete($paciente)) {
            $this->Flash->success(__('The paciente has been deleted.'));
        } else {
            $this->Flash->error(__('The paciente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	
	 public function deleteRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no borrado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
        //$this->request->allowMethod(['post', 'delete']);
		
		if ($this->request->is(['post', 'delete'])) {
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para borrar un registro debe enviar el respectivo ID';
				goto EndDeleteRest;
			}
					
			$paciente = $this->Pacientes->get($this->request->data["id"]);
			
			try{
				
				if ($this->Pacientes->delete($paciente)) {
					$data["msg"] = "Registro borrado correctamente";
					$data["result"] = true;
				} else {
					$data["msg"] = "Registro no borrado";
				}
				
			} catch (\Exception $ex) {
				
				if($ex->getCode() == 23000) {
					$data["msg"] = "Error: no puede borrar este registro, esta referenciado en otra tabla";
				} else {
					$data["msg"] = "Error: ".$ex->getCode();
				}
				
			}
			
			EndDeleteRest:
			echo json_encode($data);
		}
	
        die();
    }
}
