<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Consultorios Controller
 *
 * @property \App\Model\Table\ConsultoriosTable $Consultorios
 *
 * @method \App\Model\Entity\Especialidade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConsultoriosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
		$this->paginate = [
            'limit' => 10,
            'order' => [
                'Consultorios.id' => 'desc'
            ]
        ];
		
        $this->response->header('Access-Control-Allow-Origin', '*');
        $consultoriosObj = $this->Consultorios->find("all", ['limit' => 10]);

	
		$this->set('consultorios', $this->paginate($consultoriosObj));
		
        
        $this->set('_serialize', ['consultorios']);
    }

    /**
     * View method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewRest($id = null)
    {
		$this->paginate = [
            'limit' => 1,
            'order' => [
                'Consultorios.id' => 'desc'
            ]
        ];
		
		$this->response->header('Access-Control-Allow-Origin', '*');
		 
        $consultorio = $this->Consultorios->find("all")->where(["id"=>$id])->hydrate(false);

        $this->set('consultorios', $this->paginate($consultorio));
		$this->set('_serialize', ['consultorios']);
    }
	
	 public function view($id = null)
    {
        $consultorio = $this->Consultorios->get($id, [
            'contain' => []
        ]);

        $this->set('consultorio', $consultorio);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $consultorio = $this->Consultorios->newEntity();
        if ($this->request->is('post')) {
            $consultorio = $this->Consultorios->patchEntity($consultorio, $this->request->getData());
            if ($this->Consultorios->save($consultorio)) {
                $this->Flash->success(__('The consultorio has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The consultorio could not be saved. Please, try again.'));
        }
        $this->set(compact('consultorio'));
    }
	
	public function addRest(){
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		$consultorio = $this->Consultorios->newEntity();
       
	   if ($this->request->is('post')) {
		   
		   $consultorio = $this->Consultorios->patchEntity($consultorio, $this->request->getData());
			
		   $countErrores = count($consultorio->errors());
		 //  $errores = [];
		   if($countErrores > 0) {
			  // $data["errors"] = $consultorio->errors();
			   
			   foreach($consultorio->errors() as $key => $value) {
					foreach($value as $k => $v) {
						//$errores[$key] = $v;
						$data["msg"] = $v;
						break;
					}
			   }		
		   } else {		   
				if ($this->Consultorios->save($consultorio)) {
					$data["msg"] = "Registro guardado";
					$data["result"] = true;
				}
		   }
			echo json_encode($data);
        }
		
		die();
	}
	

    /**
     * Edit method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $consultorio = $this->Consultorios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $consultorio = $this->Consultorios->patchEntity($consultorio, $this->request->getData());
            if ($this->Consultorios->save($consultorio)) {
                $this->Flash->success(__('The consultorio has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The consultorio could not be saved. Please, try again.'));
        }
        $this->set(compact('consultorio'));
    }
	
	public function editRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no guardado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		if ($this->request->is(['patch', 'post', 'put'])) {
			
			//print_r($this->request->data);
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para editar un registro debe enviar el respectivo Id';
				goto EndEditRest;
			}
		
			$consultorio = $this->Consultorios->get($this->request->data["id"]);		
		
			$consultorio = $this->Consultorios->patchEntity($consultorio, $this->request->getData());				
			$countErrores = count($consultorio->errors());

			if($countErrores > 0) {	 
			
				foreach($consultorio->errors() as $key => $value) {
					foreach($value as $k => $v) {
						$data["msg"] = $v;
						break;
					}
				}	
				
			} else {
				    
				if ($this->Consultorios->save($consultorio)) {
					
					$data["msg"] = 'Registro editado correctamente';
					$data["result"] = true;
					
				} else {
					
					$data["msg"] = 'Registro no editado';
					
				}			
			}
			
			EndEditRest:
			echo json_encode($data);		
		}
		
		die();
    }

    /**
     * Delete method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $consultorio = $this->Consultorios->get($id);
        if ($this->Consultorios->delete($consultorio)) {
            $this->Flash->success(__('The consultorio has been deleted.'));
        } else {
            $this->Flash->error(__('The consultorio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	
	 public function deleteRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no borrado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
        //$this->request->allowMethod(['post', 'delete']);
		
		if ($this->request->is(['post', 'delete'])) {
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para borrar un registro debe enviar el respectivo ID';
				goto EndDeleteRest;
			}
					
			$consultorio = $this->Consultorios->get($this->request->data["id"]);
			
			try{
				
				if ($this->Consultorios->delete($consultorio)) {
					$data["msg"] = "Registro borrado correctamente";
					$data["result"] = true;
				} else {
					$data["msg"] = "Registro no borrado";
				}
				
			} catch (\Exception $ex) {
				
				if($ex->getCode() == 23000) {
					$data["msg"] = "Error: no puede borrar este registro, esta referenciado en otra tabla";
				} else {
					$data["msg"] = "Error: ".$ex->getCode();
				}
				
			}
			
			EndDeleteRest:
			echo json_encode($data);
		}
	
        die();
    }
}
