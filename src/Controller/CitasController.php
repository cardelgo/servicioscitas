<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Citas Controller
 *
 * @property \App\Model\Table\CitasTable $Citas
 *
 * @method \App\Model\Entity\Especialidade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CitasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
		$this->paginate = [
            'limit' => 10,
            'order' => [
                'Citas.id' => 'desc'
            ]
        ];
		
        $this->response->header('Access-Control-Allow-Origin', '*');
        $citasObj = $this->Citas->find("all", ['limit' => 10,  'contain' => ['Medicos', 'Pacientes', 'Consultorios']]);

	
		$this->set('citas', $this->paginate($citasObj));
		
        
        $this->set('_serialize', ['citas']);
    }

    /**
     * View method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewRest($id = null)
    {
		$this->paginate = [
            'limit' => 1,
            'order' => [
                'Citas.id' => 'desc'
            ]
        ];
		
		$this->response->header('Access-Control-Allow-Origin', '*');
		 
        $cita = $this->Citas->find("all", ['contain' => ['Medicos', 'Pacientes', 'Consultorios']])->where(["id"=>$id])->hydrate(false);

        $this->set('citas', $this->paginate($cita));
		$this->set('_serialize', ['citas']);
    }
	
	  public function view($id = null)
    {
        $cita = $this->Citas->get($id, [
            'contain' => ['Medicos', 'Pacientes', 'Consultorios']
        ]);

        $this->set('cita', $cita);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
   public function add()
    {
        $cita = $this->Citas->newEntity();
        if ($this->request->is('post')) {
            $cita = $this->Citas->patchEntity($cita, $this->request->getData());
            if ($this->Citas->save($cita)) {
                $this->Flash->success(__('The cita has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cita could not be saved. Please, try again.'));
        }
        $medicos = $this->Citas->Medicos->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "full_name"]);
        $pacientes = $this->Citas->Pacientes->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "full_name"]);
        $consultorios = $this->Citas->Consultorios->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "descripcion"]);
        $this->set(compact('cita', 'medicos', 'pacientes', 'consultorios'));
    }
	
	public function addRest(){
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
	   if ($this->request->is('post')) {
		   
		   $cita = $this->Citas->newEntity();
		   $cita = $this->Citas->patchEntity($cita, $this->request->getData());
			
		   $countErrores = count($cita->errors());
		  // print_r($cita = $this->Citas->newEntity());
		 //  $errores = [];
		   if($countErrores > 0) {
			$data["errors"] = $cita->errors();

			//print_r( $data["errors"]);
			  
			   foreach($cita->errors() as $key => $value) {
					foreach($value as $k => $v) {
						//$errores[$key] = $v;
						$data["msg"] = $v;
						break;
					}
			   }		
		   } else {		
			
				try {
					if ($this->Citas->save($cita)) {
						$data["msg"] = "Registro guardado";
						$data["result"] = true;
					}
				} catch(\Exception $ex) {
					if( $ex->getCode() == "23000") {
						$data["msg"] = "Error: verifica que los valores de los campos foraneos (medico_id, paciente_id, consultorio_id), ";
						$data["msg"] .= "correspondan a registros almacenados en sus respectivas tablas";
					} else {
						$data["msg"] = $ex->getMessage();
					}
				}
		   }
			echo json_encode($data);
        }
		
		die();
	}
	
	

    /**
     * Edit method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cita = $this->Citas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cita = $this->Citas->patchEntity($cita, $this->request->getData());
            if ($this->Citas->save($cita)) {
                $this->Flash->success(__('The cita has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cita could not be saved. Please, try again.'));
        }
        $medicos = $this->Citas->Medicos->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "full_name"]);
        $pacientes = $this->Citas->Pacientes->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "full_name"]);
        $consultorios = $this->Citas->Consultorios->find('list', ['limit' => 200, 'keyField' => "id",  'valueField' => "descripcion"]);
        $this->set(compact('cita', 'medicos', 'pacientes', 'consultorios'));
    }
	
	public function editRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no guardado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		if ($this->request->is(['patch', 'post', 'put'])) {
			
			//print_r($this->request->data);
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para editar un registro debe enviar el respectivo Id';
				goto EndEditRest;
			}
		
			$cita = $this->Citas->get($this->request->data["id"]);		
		
			$cita = $this->Citas->patchEntity($cita, $this->request->getData());				
			$countErrores = count($cita->errors());

			if($countErrores > 0) {	 
			
				foreach($cita->errors() as $key => $value) {
					foreach($value as $k => $v) {
						$data["msg"] = $v;
						break;
					}
				}	
				
			} else {
				    
				try {
					if ($this->Citas->save($cita)) {
						$data["msg"] = "Registro editado";
						$data["result"] = true;
					}
				} catch(\Exception $ex) {
					if( $ex->getCode() == "23000") {
						$data["msg"] = "Error: verifica que los valores de los campos foraneos (medico_id, paciente_id, consultorio_id), ";
						$data["msg"] .= "correspondan a registros almacenados en sus respectivas tablas";
					} else {
						$data["msg"] = $ex->getMessage();
					}
				}	
			}
			
			EndEditRest:
			echo json_encode($data);		
		}
		
		die();
    }

    /**
     * Delete method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cita = $this->Citas->get($id);
        if ($this->Citas->delete($cita)) {
            $this->Flash->success(__('The cita has been deleted.'));
        } else {
            $this->Flash->error(__('The cita could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	
	 public function deleteRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no borrado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
        //$this->request->allowMethod(['post', 'delete']);
		
		if ($this->request->is(['post', 'delete'])) {
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para borrar un registro debe enviar el respectivo ID';
				goto EndDeleteRest;
			}
					
			$cita = $this->Citas->get($this->request->data["id"]);
			
			try{
				
				if ($this->Citas->delete($cita)) {
					$data["msg"] = "Registro borrado correctamente";
					$data["result"] = true;
				} else {
					$data["msg"] = "Registro no borrado";
				}
				
			} catch (\Exception $ex) {
				
				if($ex->getCode() == 23000) {
					$data["msg"] = "Error: no puede borrar este registro, esta referenciado en otra tabla";
				} else {
					$data["msg"] = "Error: ".$ex->getCode();
				}
				
			}
			
			EndDeleteRest:
			echo json_encode($data);
		}
	
        die();
    }
}
