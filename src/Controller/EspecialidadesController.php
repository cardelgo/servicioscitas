<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Especialidades Controller
 *
 * @property \App\Model\Table\EspecialidadesTable $Especialidades
 *
 * @method \App\Model\Entity\Especialidade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EspecialidadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
		$this->paginate = [
            'limit' => 10,
            'order' => [
                'Especialidades.id' => 'desc'
            ]
        ];
		
        $this->response->header('Access-Control-Allow-Origin', '*');
        $especialidadesObj = $this->Especialidades->find("all", ['limit' => 10]);

	
		$this->set('especialidades', $this->paginate($especialidadesObj));
		
        
        $this->set('_serialize', ['especialidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewRest($id = null)
    {
		$this->paginate = [
            'limit' => 1,
            'order' => [
                'Especialidades.id' => 'desc'
            ]
        ];
		
		$this->response->header('Access-Control-Allow-Origin', '*');
		 
        $especialidade = $this->Especialidades->find("all")->where(["id"=>$id])->hydrate(false);

        $this->set('especialidades', $this->paginate($especialidade));
		$this->set('_serialize', ['especialidades']);
    }
	
	 public function view($id = null)
    {
        $especialidade = $this->Especialidades->get($id, [
            'contain' => []
        ]);

        $this->set('especialidade', $especialidade);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $especialidade = $this->Especialidades->newEntity();
        if ($this->request->is('post')) {
            $especialidade = $this->Especialidades->patchEntity($especialidade, $this->request->getData());
            if ($this->Especialidades->save($especialidade)) {
                $this->Flash->success(__('The especialidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The especialidade could not be saved. Please, try again.'));
        }
        $this->set(compact('especialidade'));
    }
	
	public function addRest(){
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		$especialidade = $this->Especialidades->newEntity();
       
	   if ($this->request->is('post')) {
		   
		   $especialidade = $this->Especialidades->patchEntity($especialidade, $this->request->getData());
			
		   $countErrores = count($especialidade->errors());
		 //  $errores = [];
		   if($countErrores > 0) {
			  // $data["errors"] = $especialidade->errors();
			   
			   foreach($especialidade->errors() as $key => $value) {
					foreach($value as $k => $v) {
						//$errores[$key] = $v;
						$data["msg"] = $v;
						break;
					}
			   }		
		   } else {		   
				if ($this->Especialidades->save($especialidade)) {
					$data["msg"] = "Registro guardado";
					$data["result"] = true;
				}
		   }
			echo json_encode($data);
        }
		
		die();
	}
	
	public function obtenerEspPorDescRest($descripcion) {

        $this->paginate = [
            'limit' => 10,
            'order' => [
                'Especialidades.id' => 'desc'
            ]
        ];
        $this->response->header('Access-Control-Allow-Origin', '*');

        $especialidadesObj = $this->Especialidades->find("all", ['limit' => 10])
                ->where(["Especialidades.descripcion LIKE " => '%' . $descripcion . '%'])
                ->hydrate(false);


        $this->set('especialidades', $this->paginate($especialidadesObj));
        $this->set('_serialize', ['especialidades']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $especialidade = $this->Especialidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $especialidade = $this->Especialidades->patchEntity($especialidade, $this->request->getData());
            if ($this->Especialidades->save($especialidade)) {
                $this->Flash->success(__('The especialidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The especialidade could not be saved. Please, try again.'));
        }
        $this->set(compact('especialidade'));
    }
	
	public function editRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no guardado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
		if ($this->request->is(['patch', 'post', 'put'])) {
			
			//print_r($this->request->data);
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para editar un registro debe enviar el respectivo Id';
				goto EndEditRest;
			}
		
			$especialidade = $this->Especialidades->get($this->request->data["id"]);		
		
			$especialidade = $this->Especialidades->patchEntity($especialidade, $this->request->getData());				
			$countErrores = count($especialidade->errors());

			if($countErrores > 0) {	 
			
				foreach($especialidade->errors() as $key => $value) {
					foreach($value as $k => $v) {
						$data["msg"] = $v;
						break;
					}
				}	
				
			} else {
				    
				if ($this->Especialidades->save($especialidade)) {
					
					$data["msg"] = 'Registro editado correctamente';
					$data["result"] = true;
					
				} else {
					
					$data["msg"] = 'Registro no editado';
					
				}			
			}
			
			EndEditRest:
			echo json_encode($data);		
		}
		
		die();
    }

    /**
     * Delete method
     *
     * @param string|null $id Especialidade id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $especialidade = $this->Especialidades->get($id);
        if ($this->Especialidades->delete($especialidade)) {
            $this->Flash->success(__('The especialidade has been deleted.'));
        } else {
            $this->Flash->error(__('The especialidade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	
	 public function deleteRest()
    {
		$this->autoRender = false;
        $this->viewBuilder()->layout("");
		
		$data["result"] = false;
		$data["msg"] = "Registro no borrado";
		
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
		
        //$this->request->allowMethod(['post', 'delete']);
		
		if ($this->request->is(['post', 'delete'])) {
			
			if(!isset( $this->request->data["id"]) ){
				$data["msg"] = 'Para borrar un registro debe enviar el respectivo ID';
				goto EndDeleteRest;
			}
					
			$especialidade = $this->Especialidades->get($this->request->data["id"]);
			
			try{
				
				if ($this->Especialidades->delete($especialidade)) {
					$data["msg"] = "Registro borrado correctamente";
					$data["result"] = true;
				} else {
					$data["msg"] = "Registro no borrado";
				}
				
			} catch (\Exception $ex) {
				
				if($ex->getCode() == 23000) {
					$data["msg"] = "Error: no puede borrar este registro, esta referenciado en otra tabla";
				} else {
					$data["msg"] = "Error: ".$ex->getCode();
				}
				
			}
			
			EndDeleteRest:
			echo json_encode($data);
		}
	
        die();
    }
}
