<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('base.css') ?>
        <?= $this->Html->css('cake.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <nav class="top-bar expanded" data-topbar role="navigation">
            <ul class="title-area large-3 medium-4 columns">
                <li class="name">
                    <h1><a href=""><?= $this->fetch('title') ?></a></h1>
                </li>
            </ul>
            <div class="top-bar-section">
                <ul class="right">
                    <li><a target="_blank" href="https://book.cakephp.org/3.0/">Documentation</a></li>
                    <li><a target="_blank" href="https://api.cakephp.org/3.0/">API</a></li>
                </ul>
            </div>
        </nav>

        <div class="container clearfix">
            <nav class="large-3 medium-4 columns" id="actions-sidebar">
                <ul class="side-nav">
                    <li class="heading"><?= __('Menu principal') ?></li>
                    <hr />
                    <li><?= $this->Html->link(__('Agendar cita'), ['controller' => 'citas', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('Listado Citas'), ['controller' => 'citas', 'action' => 'index']) ?></li>
                    <hr />
                    <li><?= $this->Html->link(__('Agregar medico'), ['controller' => 'medicos', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('Listado medicos'), ['controller' => 'medicos', 'action' => 'index']) ?></li>
                    <hr />
                    <li><?= $this->Html->link(__('Agregar paciente'), ['controller' => 'pacientes', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('Listado pacientes'), ['controller' => 'pacientes', 'action' => 'index']) ?></li>
                    <hr />
                    <li><?= $this->Html->link(__('Agregar consultorio'), ['controller' => 'consultorios', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('Listado consultorios'), ['controller' => 'consultorios', 'action' => 'index']) ?></li>
                    <hr />
                    <li><?= $this->Html->link(__('Agregar especialidad'), ['controller' => 'especialidades', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('Listado especialidades'), ['controller' => 'especialidades', 'action' => 'index']) ?></li>
                </ul>
            </nav>
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
        <footer>
        </footer>
    </body>
</html>
