<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Especialidade $especialidade
 */
?>

<div class="especialidades form large-9 medium-8 columns content">
    <?= $this->Form->create($especialidade) ?>
    <fieldset>
        <legend><?= __('Add Especialidad') ?></legend>
        <?php
            echo $this->Form->control('descripcion');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
