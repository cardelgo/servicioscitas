<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cita $cita
 */
?>

<div class="citas view large-9 medium-8 columns content">
    <h3><?= h($cita->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Medico') ?></th>
            <td><?= $cita->has('medico') ? $this->Html->link($cita->medico->full_name, ['controller' => 'Medicos', 'action' => 'view', $cita->medico->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Paciente') ?></th>
            <td><?= $cita->has('paciente') ? $this->Html->link($cita->paciente->full_name, ['controller' => 'Pacientes', 'action' => 'view', $cita->paciente->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Consultorio') ?></th>
            <td><?= $cita->has('consultorio') ? $this->Html->link($cita->consultorio->descripcion, ['controller' => 'Consultorios', 'action' => 'view', $cita->consultorio->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comentarios') ?></th>
            <td><?= h($cita->comentarios) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cita->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora') ?></th>
            <td><?= h($cita->hora) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($cita->fecha) ?></td>
        </tr>
    </table>
</div>
