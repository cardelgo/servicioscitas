<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cita[]|\Cake\Collection\CollectionInterface $citas
 */
?>

<div class="citas index large-9 medium-8 columns content">
    <h3><?= __('Citas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('medico_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paciente_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('consultorio_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comentarios') ?></th>
                <th scope="col"><?= $this->Paginator->sort('hora') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($citas as $cita): ?>
            <tr>
                <td><?= $this->Number->format($cita->id) ?></td>
                <td><?= $cita->has('medico') ? $this->Html->link($cita->medico->full_name, ['controller' => 'Medicos', 'action' => 'view', $cita->medico->id]) : '' ?></td>
                <td><?= $cita->has('paciente') ? $this->Html->link($cita->paciente->full_name, ['controller' => 'Pacientes', 'action' => 'view', $cita->paciente->id]) : '' ?></td>
                <td><?= $cita->has('consultorio') ? $this->Html->link($cita->consultorio->descripcion, ['controller' => 'Consultorios', 'action' => 'view', $cita->consultorio->id]) : '' ?></td>
                <td><?= h($cita->comentarios) ?></td>
                <td><?= h($cita->hora) ?></td>
                <td><?= h($cita->fecha) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $cita->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cita->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cita->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cita->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
