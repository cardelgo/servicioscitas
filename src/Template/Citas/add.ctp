<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cita $cita
 */
?>

<div class="citas form large-9 medium-8 columns content">
    <?= $this->Form->create($cita) ?>
    <fieldset>
        <legend><?= __('Add Cita') ?></legend>
        <?php
            echo $this->Form->control('medico_id', ['options' => $medicos]);
            echo $this->Form->control('paciente_id', ['options' => $pacientes]);
            echo $this->Form->control('consultorio_id', ['options' => $consultorios]);
            echo $this->Form->control('comentarios');
            echo $this->Form->control('hora', ['empty' => true]);
            echo $this->Form->control('fecha', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
