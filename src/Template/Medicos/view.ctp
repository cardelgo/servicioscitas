<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Medico $medico
 */
?>

<div class="medicos view large-9 medium-8 columns content">
    <h3><?= h($medico->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cedula') ?></th>
            <td><?= h($medico->cedula) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombres') ?></th>
            <td><?= h($medico->nombres) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellidos') ?></th>
            <td><?= h($medico->apellidos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Especialidad') ?></th>
            <td><?= $medico->has('Especialidad') ? $this->Html->link($medico->Especialidad->descripcion, ['controller' => 'Especialidades', 'action' => 'view', $medico->Especialidad->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($medico->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Citas') ?></h4>
        <?php if (!empty($medico->citas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Medico Id') ?></th>
                <th scope="col"><?= __('Paciente Id') ?></th>
                <th scope="col"><?= __('Consultorio Id') ?></th>
                <th scope="col"><?= __('Comentarios') ?></th>
                <th scope="col"><?= __('Hora') ?></th>
                <th scope="col"><?= __('Fecha') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($medico->citas as $citas): ?>
            <tr>
                <td><?= h($citas->id) ?></td>
                <td><?= h($citas->medico_id) ?></td>
                <td><?= h($citas->paciente_id) ?></td>
                <td><?= h($citas->consultorio_id) ?></td>
                <td><?= h($citas->comentarios) ?></td>
                <td><?= h($citas->hora) ?></td>
                <td><?= h($citas->fecha) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Citas', 'action' => 'view', $citas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Citas', 'action' => 'edit', $citas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Citas', 'action' => 'delete', $citas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $citas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
