<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Consultorio[]|\Cake\Collection\CollectionInterface $consultorios
 */
?>

<div class="consultorios index large-9 medium-8 columns content">
    <h3><?= __('Consultorios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descripcion') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($consultorios as $consultorio): ?>
            <tr>
                <td><?= $this->Number->format($consultorio->id) ?></td>
                <td><?= h($consultorio->descripcion) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $consultorio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $consultorio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $consultorio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $consultorio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
