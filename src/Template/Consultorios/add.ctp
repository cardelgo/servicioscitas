<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Consultorio $consultorio
 */
?>

<div class="consultorios form large-9 medium-8 columns content">
    <?= $this->Form->create($consultorio) ?>
    <fieldset>
        <legend><?= __('Add Consultorio') ?></legend>
        <?php
            echo $this->Form->control('descripcion');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
