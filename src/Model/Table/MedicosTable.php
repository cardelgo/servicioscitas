<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Medicos Model
 *
 * @property \App\Model\Table\EspecialidadesTable|\Cake\ORM\Association\BelongsTo $Especialidades
 * @property \App\Model\Table\CitasTable|\Cake\ORM\Association\HasMany $Citas
 *
 * @method \App\Model\Entity\Medico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Medico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Medico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Medico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Medico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Medico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Medico findOrCreate($search, callable $callback = null, $options = [])
 */
class MedicosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('medicos');
        $this->setDisplayField('full_name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Especialidades', [
            'foreignKey' => 'especialidad_id',
            'joinType' => 'INNER',
			'propertyName' => 'Especialidad'
        ]);
        $this->hasMany('Citas', [
            'foreignKey' => 'medico_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('cedula')
            ->maxLength('cedula', 45)
            ->requirePresence('cedula', 'create')
            ->notEmpty('cedula', "Debe llenar el campo cedula")
            ->add('cedula', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 
			"message"=>"La cedula ingresada ya esta registrada"]);

        $validator
            ->scalar('nombres')
            ->maxLength('nombres', 45)
            ->requirePresence('nombres', 'create')
            ->notEmpty('nombres', "Debe llenar el campo nombres");

        $validator
            ->scalar('apellidos')
            ->maxLength('apellidos', 45)
            ->requirePresence('apellidos', 'create')
            ->notEmpty('apellidos',"Debe llenar el campo apellidos");

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['cedula']));
        $rules->add($rules->existsIn(['especialidad_id'], 'Especialidades', "Debe enviar una especialidad existente"));

        return $rules;
    }
}
