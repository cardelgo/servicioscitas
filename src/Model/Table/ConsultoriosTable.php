<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Consultorios Model
 *
 * @property \App\Model\Table\CitasTable|\Cake\ORM\Association\HasMany $Citas
 *
 * @method \App\Model\Entity\Consultorio get($primaryKey, $options = [])
 * @method \App\Model\Entity\Consultorio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Consultorio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Consultorio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Consultorio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Consultorio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Consultorio findOrCreate($search, callable $callback = null, $options = [])
 */
class ConsultoriosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('consultorios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Citas', [
            'foreignKey' => 'consultorio_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('descripcion')
            ->maxLength('descripcion', 45)
            ->requirePresence('descripcion', 'create')
            ->notEmpty('descripcion')
            ->add('descripcion', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->isUnique(['descripcion']));

        return $rules;
    }
}
