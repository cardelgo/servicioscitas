<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cita Entity
 *
 * @property int $id
 * @property int $medico_id
 * @property int $paciente_id
 * @property int $consultorio_id
 * @property string $comentarios
 * @property \Cake\I18n\FrozenTime $hora
 * @property \Cake\I18n\FrozenDate $fecha
 *
 * @property \App\Model\Entity\Medico $medico
 * @property \App\Model\Entity\Paciente $paciente
 * @property \App\Model\Entity\Consultorio $consultorio
 */
class Cita extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'medico_id' => true,
        'paciente_id' => true,
        'consultorio_id' => true,
        'comentarios' => true,
        'hora' => true,
        'fecha' => true,
        'medico' => true,
        'paciente' => true,
        'consultorio' => true
    ];
}
