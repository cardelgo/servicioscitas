<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Medico Entity
 *
 * @property int $id
 * @property string $cedula
 * @property string $nombres
 * @property string $apellidos
 * @property int $especialidad_id
 *
 * @property \App\Model\Entity\Especialidade $especialidade
 * @property \App\Model\Entity\Cita[] $citas
 */
class Medico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cedula' => true,
        'nombres' => true,
        'apellidos' => true,
        'especialidad_id' => true,
        'especialidade' => true,
        'citas' => true
    ];
	
	protected function _getFullName()
    {
        return $this->nombres . '  ' . $this->apellidos. ', CC: ' . $this->cedula;
    } 
}
